#include "xmlparser.hpp"

XmlParser::XmlParser(const std::string& filename)
    : filename_(filename)
{}

void XmlParser::parse()
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(filename_.c_str());

    if (!result)
    {
        throw std::runtime_error("Error parsing XML file");
    }

    for (const auto& point_node : doc.select_nodes("//Point"))
    {
        Point point;
        point.latitude = point_node.node().select_node("Position/Latitude").node().text().as_double();
        point.longitude = point_node.node().select_node("Position/Longitude").node().text().as_double();
        point.altitude = point_node.node().select_node("Position/Altitude").node().text().as_double();
        points_.push_back(point);
    }
}

const std::vector<Point>& XmlParser::getPoints() const
{
    return points_;
}