#ifndef XMLPARSER_HPP
#define XMLPARSER_HPP

#include <vector>
#include <string>
#include "pugixml.hpp"

struct Point {
    double latitude;
    double longitude;
    double altitude;
};

class XmlParser {
public:
    XmlParser(const std::string& filename);
    void parse();
    const std::vector<Point>& getPoints() const;
private:
    std::string filename_;
    std::vector<Point> points_;
};

#endif