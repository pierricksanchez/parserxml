#include <iostream>
#include <fstream>
#include <ctime>
#include "xmlparser.hpp"

int main()
{
    XmlParser parser("example.xml");
    parser.parse();

    std::ofstream output_file("output.txt", std::ios_base::app); // ouverture en mode "ajout" pour incrémenter les données
    if (!output_file.is_open()) {
        std::cerr << "Failed to open output file." << std::endl;
        return 1;
    }

    // obtenir la date et l'heure actuelles
    std::time_t current_time = std::time(nullptr);
    std::tm* local_time = std::localtime(&current_time);
    char date_str[11];
    std::strftime(date_str, sizeof(date_str), "%d/%m/%Y", local_time);
    char time_str[9];
    std::strftime(time_str, sizeof(time_str), "%H:%M:%S", local_time);

    // // écrire les headers de date et d'heure dans le fichier
    // output_file << "Date\t" << "Heure\t" << "Latitude\tLongitude\tAltitude\n";
    // output_file << date_str << "\t" << time_str << "\n";

    // écrire les données dans le fichier
    for (const auto& point : parser.getPoints())
    {
        output_file << date_str << "\t" << time_str << "\t" << point.latitude << "\t" << point.longitude << "\t" << point.altitude << "\n";
    }

    output_file.close();

    std::cout << "Parsing complete. Data saved to output.txt." << std::endl;
    return 0;
}